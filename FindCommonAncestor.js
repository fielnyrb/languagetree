﻿function getCommonAncestor(language1, language2) {
    if (language1 === language2) {
        return language1;
    }

    var relation = _findRelation(language1, language2);
    if (relation !== false) {
        return relation;
    } else {
        var swappedLanguages = _swapLanguages(language1, language2);
        return _findRelation(swappedLanguages['language1'], swappedLanguages['language2']);
    }

    return false;
}

function _swapLanguages(lan1, lan2) {
    return { language1: lan2, language2: lan1 };
}

function _findRelation(language1, language2) {
    const languageRelations = {
        'faroese':
        {
            'danish': 'northgermanic',
            'norwegian': 'northgermanic',
            'swedish': 'northgermanic',
            'icelandic': 'northgermanic',
        }
    };

    if (typeof languageRelations[language1] !== 'undefined' && typeof languageRelations[language1][language2] !== 'undefined') {
        return languageRelations[language1][language2];
    }
    return false;
}