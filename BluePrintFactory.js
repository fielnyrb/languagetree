function getBluePrint(tree) {

    var currentDetail = tree;

    return buildBlueprint(currentDetail);

}

function buildBlueprint(tree) {
    let bluePrint = [];

    //https://www.geeksforgeeks.org/generic-tree-level-order-traversal/
    let q = [];
    q.push(tree);

    while (q.length !== 0) {
        let n = q.length;
        
        while (n > 0) {
            let p = q.shift();

            if (p.descendants.length === 0) {
                bluePrint.push(p.currentBlueprint);
            }

            for (let i = 0; i < p.descendants.length; i++) {
                q.push(p.descendants[i]);
            }
            n--;
            
        }
    }

    return bluePrint;
}